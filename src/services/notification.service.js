const axios = require('axios').default;
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

/**
 * Send a notifiction
 * @param {string} type
 * @param {string} adminId
 * @returns {Promise<boolean>}
 */
const sendNotification = async (type, adminId) => {
  console.log('sending notification: ', { type, id: adminId });
  const response = (await axios.post(process.env.NOTIFICATIONS_URL, { type, id: adminId })).data;
  if (response.status !== 'ok') {
    throw new ApiError(
      httpStatus.INTERNAL_SERVER_ERROR,
      'Error sending notification to admin',
      'Error enviando notificación al admin'
    );
  }
};

module.exports = {
  sendNotification,
}
