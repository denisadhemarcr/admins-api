const httpStatus = require('http-status');
const { Admin } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a admin
 * @param {Object} adminBody
 * @returns {Promise<Admin>}
 */
const createAdmin = async (adminBody) => {
  if (await Admin.isEmailTaken(adminBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken', 'El email ya está en uso');
  }
  return Admin.create(adminBody);
};


/**
 * Query for admins
 * @param {Object} filter - Mongo filter
 * @returns {Promise<QueryResult>}
 */
const listAdmins = async () => {
  const admins = await Admin.find({ $or: [{ status: 'pending' }, { status: 'active' }] }).exec();
  return { admins };
};


/**
 * Get admin by id
 * @param {ObjectId} adminId
 * @returns {Promise<Admin>}
 */
const getAdminById = async (adminId) => {
  return Admin.findById(adminId);
};

/**
 * Update admin by id
 * @param {ObjectId} adminId
 * @param {Object} updateBody
 * @returns {Promise<Admin>}
 */
const updateAdminById = async (adminId, updateBody) => {
  let notification;
  const admin = await getAdminById(adminId);
  if (!admin) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Resource not found', 'Recurso no encontrado');
  }
  if (updateBody.email) {
    if (admin.status !== "pending") {
      throw new ApiError(
        httpStatus.BAD_REQUEST,
        `Cannot edit email of admin which status is ${admin.status}`,
        `No se puede editar el email de un Admin con status ${admin.status}`
      );
    }
    if (await Admin.isEmailTaken(updateBody.email, adminId)) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken', 'El email ya está en uso');
    }
  }

  if (updateBody.status === 'active' && admin.status === 'pending') {
    notification = 'confirm';
  };
  if (updateBody.status === 'disable' && admin.status === 'pending') {
    notification = 'reject';
  };

  Object.assign(admin, updateBody);
  await admin.save();

  return { admin, notification };
};

/**
 * Delete admin by id
 * @param {ObjectId} adminId
 * @returns {Promise<Admin>}
 */
const deleteAdminById = async (adminId) => {
  const admin = await getAdminById(adminId);
  if (!admin) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Resource not found', 'Recurso no encontrado');
  }
  if (admin.status === 'pending') {
    await admin.remove();
  } else {
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      `Cannot delete admin which status is ${admin.status}`,
      `No se puede eliminar un Admin cuyo status es ${admin.status}`
    );
  }
  return admin;
};


module.exports = {
  createAdmin,
  listAdmins,
  updateAdminById,
  deleteAdminById,
}
