const Joi = require('joi');
const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');

const validateRequest = (schema) => (req, _res, next) => {
  const validSchema = pick(schema, ['params', 'query', 'body']);
  const object = pick(req, Object.keys(validSchema));
  const { value, error } = Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' }, abortEarly: false })
    .validate(object);

  if (error) {
    const errors = error.details.map((details) => details.message).join(', ');
    console.log({ errors });
    return next(new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'Invalid attribute', 'Atributo inválido'));
  }
  Object.assign(req, value);
  return next();
};

const validateMongoId = (req, _res, next) => {
  const adminId = req.params.id;
  if (!adminId.match(/^[0-9a-fA-F]{24}$/)) { // id doesn't match the mongo objectId format
    return next(new ApiError(httpStatus.BAD_REQUEST, 'Invalid admin ID', 'El ID consultado es inválido'));
  }
  return next();
};

module.exports = {
  validateRequest,
  validateMongoId,
};
