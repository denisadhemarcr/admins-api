const express = require('express');
const { validateRequest, validateMongoId } = require('../../middlewares/validate');
const { adminValidation } = require('../../validations');
const { adminController } = require('../../controllers');

const router = express.Router();

router
  .route('/')
  .post(validateRequest(adminValidation.createAdmin), adminController.createAdmin)
  .get(adminController.getAdmins);

router
  .route('/:id')
  .patch(validateMongoId, validateRequest(adminValidation.updateAdmin), adminController.updateAdmin)
  .delete(validateMongoId, adminController.deleteAdmin);


module.exports = router;
