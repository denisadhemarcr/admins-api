const express = require('express');
const adminRoute = require('./admin.route');
const router = express.Router();

router.use('/api/admin/v1', adminRoute);

module.exports = router;
