const Joi = require('joi');

const createAdmin = {
  body: Joi.object()
    .keys({
      name: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().required().email(),
      role: Joi.string().required().valid('admin_level_1', 'admin_level_2', 'admin_level_3'),
      picture: Joi.string().required(),
      // status: Joi.string().required().valid('pending', 'active', 'disable'),
    }),
};


const updateAdmin = {
  body: Joi.object()
    .keys({
      email: Joi.string().email(),
      name: Joi.string(),
      lastName: Joi.string(),
      picture: Joi.string(),
      status: Joi.string().valid('pending', 'active', 'disable'),
      role: Joi.string().valid('admin_level_1', 'admin_level_2', 'admin_level_3'),
    })
    .min(1),
};

module.exports = {
  createAdmin,
  updateAdmin,
};
