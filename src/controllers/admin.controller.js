const catchAsync = require('../utils/catchAsync');
const { adminService, notificationService } = require('../services');

const createAdmin = catchAsync(async (req, res) => {
  const admin = await adminService.createAdmin(req.body);
  console.log(admin.id);
  await notificationService.sendNotification("send", admin.id);
  res.send(admin);
});

const getAdmins = catchAsync(async (req, res) => {
  const result = await adminService.listAdmins();
  res.send(result);
});

const updateAdmin = catchAsync(async (req, res) => {
  const { admin, notification } = await adminService.updateAdminById(req.params.id, req.body);
  if (notification) {
    await notificationService.sendNotification(notification, admin.id);
  }
  res.send(admin);
});

const deleteAdmin = catchAsync(async (req, res) => {
  const admin = await adminService.deleteAdminById(req.params.id);
  res.send(admin);
});

module.exports = {
  createAdmin,
  getAdmins,
  updateAdmin,
  deleteAdmin,
};
