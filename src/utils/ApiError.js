class ApiError extends Error {
  constructor(
    statusCode,
    errorMessage,
    translatedMessage = errorMessage,
    isOperational = true,
    stack = ''
  ) {
    super(errorMessage);
    this.statusCode = statusCode;
    this.isOperational = isOperational;
    this.errorMessage = errorMessage;
    this.translatedMessage = translatedMessage;

    if (stack) {
      this.stack = stack;
    } else {
      Error.captureStackTrace(this, this.constructor);
    }
  }
}

module.exports = ApiError;
