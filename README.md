# Admins API

API desarrollada en Node.js, Express, and Mongoose.


## Setup & running

Instalar las dependencias:

```bash
yarn install
```

Definir las variables de entorno:

```bash
cp .env.example .env
# abrir el archivo .env y modificar las variables
```

Ejevutar localmente:

```bash
yarn dev
```

### API Endpoints

Lista de endpoints:

**Admin routes**:\
`POST /api/admin/v1` - create a admin

```js
// body:
{
  "name": "Admin",
  "lastName": "1",
  "email": "admin1@fleetr.com",
  "role": "admin.fleetrw"
  // el campo status no se manda, y se guarda como "pending" por defecto
}
```
\
`GET /api/admin/v1` - get all admins
```js
// Response:
{
  "admins": [
    {
      "role": "admin_level_3",
      "status": "active",
      "name": "Pedro",
      "lastName": "Tres",
      "email": "dasdas@jooycar.com",
      "picture": "https://picture3.com/",
      "id": "61a536946302717633244d8a"
    },
    {
      "role": "admin_level_3",
      "status": "pending",
      "name": "Usuario",
      "lastName": "Cuatro",
      "email": "admin4@jooycar.com",
      "picture": "https://picture2.com/",
      "id": "61a53d2552964978233a3153"
    },
    {
      "role": "admin_level_2",
      "status": "pending",
      "name": "Prueba",
      "lastName": "Diez",
      "email": "prueba@jooycar.com",
      "picture": "https://picture2.com/",
      "id": "61a542d11215757932c00aa1"
    },
    {
      "role": "admin_level_2",
      "status": "pending",
      "name": "Jacinto",
      "lastName": "Diaz",
      "email": "jacidiaz@jooycar.com",
      "picture": "https://picture2.com/",
      "id": "61a542e11215757932c00aa4"
    }
  ]
}
```
\
`PATCH /api/admin/v1/:adminId` - update admin\

```js
// body:
{
  "role": "new role"
}
```
\
`DELETE /api/admin/v1/:adminId` - delete admin

```js
// Resposnse:
{
  "role": "admin_level_3",
  "status": "pending",
  "name": "Usuario",
  "lastName": "Cuatro",
  "email": "admin4@jooycar.com",
  "picture": "https://picture2.com/",
  "id": "61a53d2552964978233a3153"
},
```
